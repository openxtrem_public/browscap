<h1>OX-Browscap</h1>

<h2>About</h2>

Requires browscap/browscap-php packages, convert PHP_BrowscapINI into cache folder.
<br>Contains browscap factory : 
<pre>
$bc = \OxBrowscap\BrowscapFactory::create();
$result = $bc->getBrowser();
</pre>

<h2>How to update ?</h2>

```
git clone project
composer update
php src/update.php
git add .
git commit -m "XXX"
git push 
git tag X.X.X
git push --tags
 ```

<h2>How to tests ?</h2>

```
 vendor/bin/phpunit -c phpunit.xml
 ```

<h2>Need help ?</h2>

```
vendor/bin/browscap-php list
 ```

<h2>Issues</h2>
Add gitlab-ci.yml to auto update cache on schedules pipelines.