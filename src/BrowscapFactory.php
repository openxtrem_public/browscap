<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace OxBrowscap;

use BrowscapPHP\Browscap;
use BrowscapPHP\BrowscapUpdater;
use BrowscapPHP\Exception\ErrorCachedVersionException;
use BrowscapPHP\Exception\NoCachedVersionException;
use BrowscapPHP\Exception\NoNewVersionException;
use BrowscapPHP\Helper\Exception;
use BrowscapPHP\Helper\IniLoader;
use Monolog\Logger;
use Ox\Components\Cache\Adapters\ArrayAdapter;
use Psr\SimpleCache\CacheInterface;
abstract class BrowscapFactory
{
    public const CACHE    = 'cache/browscap.serialize';

    public static function create(string $cache_file = null)
    {
        return new Browscap(self::getCache($cache_file), self::getLogger());
    }

    private static function getCache(string $cache_file = null): CacheInterface
    {
        $cache_file = $cache_file ?? self::getRootDir(self::CACHE);
        return unserialize(file_get_contents($cache_file), [ArrayAdapter::class]);
    }

    private static function getLogger(): Logger
    {
        return new Logger('browscap');
    }

    /**
     * @param bool $force
     *
     * @return void
     * @throws Exception
     */
    public static function updateCache(bool $force = false, string $cache_file = null): void
    {
        $cache_file = $cache_file ?? self::getRootDir(self::CACHE);

        $cache   = new ArrayAdapter();
        $logger  = self::getLogger();

        if (!is_file($cache_file) || $force) {
            $updater = new BrowscapUpdater($cache, $logger);
            $updater->update(IniLoader::PHP_INI);

            file_put_contents($cache_file, serialize($cache));
        } else {
            try {
                $cache = self::getCache();
                $updater = new BrowscapUpdater($cache, $logger);
                try {
                    $updater->checkUpdate();
                } catch (NoCachedVersionException|ErrorCachedVersionException) {}

                $updater->update(IniLoader::PHP_INI);

                file_put_contents($cache_file, serialize($cache));
            } catch (NoNewVersionException) {}
        }
    }


    /**
     * @param null $concat
     *
     * @return string
     */
    public static function getRootDir($concat = null): string
    {
        $root = dirname(__DIR__);
        if ($concat) {
            $root .= DIRECTORY_SEPARATOR . $concat;
        }

        return $root;
    }
}
