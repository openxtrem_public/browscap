<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

require __DIR__ . "/../vendor/autoload.php";

use OxBrowscap\BrowscapFactory;

try {
    BrowscapFactory::updateCache(true);
    echo 'Browscap cache updated';
} catch (Exception $exception) {
    echo $exception->getMessage();
}