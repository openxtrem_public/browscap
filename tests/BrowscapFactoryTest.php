<?php

namespace OxBrowscapTest;

use BrowscapPHP\Browscap;
use BrowscapPHP\Exception;
use OxBrowscap\BrowscapFactory;
use PHPUnit\Framework\TestCase;

/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */
class BrowscapFactoryTest extends TestCase
{

    public function testCreate()
    {
        $bc = BrowscapFactory::create();
        $this->assertInstanceOf(Browscap::class, $bc);

        return $bc;
    }

    /**
     * @depends testCreate
     *
     * @param Browscap $bc
     *
     * @throws Exception
     */
    public function testGetBrowser(Browscap $bc)
    {
        $ua = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36";
        $result = $bc->getBrowser($ua);
        $this->assertEquals($result->browser, 'Chrome');
    }

    public function testGetRoot(){
        $root = BrowscapFactory::getRootDir();
        $this->assertDirectoryExists($root);
        $root_concat = BrowscapFactory::getRootDir('toto');
        $this->assertEquals($root_concat, $root.'/toto');
    }
}